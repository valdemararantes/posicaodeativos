package com.galgo.posicaodeativos;

import br.com.galgo.galgowsgenerated.GalgoServicesFactory;
import com.galgo.cxfutils.CXFUtils;
import com.galgo.cxfutils.sec.WSHeader;
import com.galgo.cxfutils.ws.WS;
import com.galgo.utils.XMLGregorianCalendarConversionUtil;
import com.sistemagalgo.schemaposicaoativos.MarkersComplexType;
import com.sistemagalgo.schemaposicaoativos.MessagePosicaoAtivosComplexType;
import com.sistemagalgo.schemaposicaoativos.MessagePosicaoAtivosConsumirComplexType;
import com.sistemagalgo.schemaposicaoativos.STIComplexType;
import com.sistemagalgo.serviceposicaoativos.Consumir;
import com.sistemagalgo.serviceposicaoativos.ConsumirFaultMsg;
import com.sistemagalgo.serviceposicaoativos.ServicePosicaoAtivos;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXB;
import javax.xml.ws.BindingProvider;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Optional;

/**
 * Created by valdemar.arantes on 01/12/2015.
 */
public class PosicaoDeAtivos extends WS {

    private static final Logger log = LoggerFactory.getLogger(PosicaoDeAtivos.class);
    private static final String SERVICE_POS_ATIVOS = "/ServicePosicaoAtivos";
    private final StringWriter sWriter;
    private MessagePosicaoAtivosConsumirComplexType request = new MessagePosicaoAtivosConsumirComplexType();

    public PosicaoDeAtivos() {
        sWriter = new StringWriter();
    }

    public PosicaoDeAtivos addCdObjetos(Integer cdSTI, Long cnpj, String isin, String sigla) {
        if (cdSTI == null && cnpj == null && isin == null && sigla == null) {
            log.warn("Método retornando sem ser executado porque todos os parâmetros são nulos");
            return this;
        }
        final STIComplexType elem = new STIComplexType();
        elem.setCdSTI(cdSTI);
        elem.setCNPJ(cnpj);
        elem.setISIN(isin);
        elem.setSigla(sigla);
        request.getCdObjetos().add(elem);
        return this;
    }

    /**
     * Consome as informações de Posicção de Ativos
     *
     * @throws ConsumirFaultMsg
     */
    public MessagePosicaoAtivosComplexType consumir() throws ConsumirFaultMsg {
        Objects.requireNonNull(user, "Login e senha do usuario nao foram definidos.");
        final MessagePosicaoAtivosComplexType response = getServicePosAtivos().consumir(request);
        log.info("Consumo ok. response {}", response == null ? "NULO" : "NÃO NULO");
        return response;
    }

    public String getRequestResponseString() {
        return sWriter.toString();
    }

    /**
     * @return A string com o trecho dentro do body do request
     */
    public String getRequestString() {
        Consumir consumir = new Consumir();
        consumir.setRequisicaoConsulta(request);
        StringWriter sw = new StringWriter();
        JAXB.marshal(consumir, sw);
        return sw.getBuffer().toString();
    }

    public PosicaoDeAtivos setDt(LocalDate dt) {
        request.setDtPosicaoAtivos(XMLGregorianCalendarConversionUtil.asXMLGregorianCalendar_Date(dt));
        return this;
    }

    public PosicaoDeAtivos setIdMsgSender(String idMsgSender) {
        request.setIdMsgSender(idMsgSender);
        return this;
    }

    public PosicaoDeAtivos setMarkers(Long assMarker, String rptMarker) {
        if (assMarker == null && StringUtils.isBlank(rptMarker)) {
            return null;
        }
        MarkersComplexType markers = new MarkersComplexType();
        Optional.of(assMarker).ifPresent(markers::setAssMarker);
        Optional.of(rptMarker).ifPresent(markers::setRptMarker);
        request.setMarcadores(markers);
        return this;
    }

    public PosicaoDeAtivos setMoeda(String moeda) {
        request.setMoeda(moeda);
        return this;
    }

    @Override
    protected String getWsUriName() {
        return SERVICE_POS_ATIVOS;
    }

    private ServicePosicaoAtivos getServicePosAtivos() {
        ServicePosicaoAtivos service = GalgoServicesFactory.newPosAtivosServiceInstance();
        setEndPoint((BindingProvider) service);
        WSHeader.buildHeader(service, user);
        CXFUtils.turnOnClientLoggingFeature(service, new PrintWriter(sWriter));
        return service;
    }
}
