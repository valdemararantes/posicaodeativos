package com.galgo.posicaodeativos;

import br.com.stianbid.common.MessageExceptionComplexType;
import com.galgo.cxfutils.ws.AmbienteEnum;
import com.galgo.utils.cert_digital.TrustAllCerts;
import com.sistemagalgo.schemaposicaoativos.BsnsMsgComplexType;
import com.sistemagalgo.schemaposicaoativos.MessagePosicaoAtivosComplexType;
import com.sistemagalgo.serviceposicaoativos.ConsumirFaultMsg;
import org.apache.commons.collections.CollectionUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXB;
import java.io.StringWriter;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by valdemar.arantes on 13/01/2016.
 */
public class PosicaoDeAtivosTest {

    private static final Logger log = LoggerFactory.getLogger(PosicaoDeAtivosTest.class);

    @BeforeClass
    public static void initTest() {
        TrustAllCerts.doIt();
    }

    @Test
    public void testConsumir() throws Exception {
        log.info("Início do teste ***************************************************************");
        final PosicaoDeAtivos posicaoDeAtivos = new PosicaoDeAtivos();
        posicaoDeAtivos.setAmbiente(AmbienteEnum.HOMOLOGACAO).setUser("SG_0409.SG_04", "Galgo111");

        try {
            //@formatter:off
            final MessagePosicaoAtivosComplexType resp = posicaoDeAtivos.
                setDt(LocalDate.of(2015, 4, 29)).
                setMoeda("BRL").
                setIdMsgSender("PosicaoDeAtivosTest").
                addCdObjetos(514373, null, null, null).
                consumir();
            //@formatter:on
            final List<BsnsMsgComplexType> bsnsMsgList = resp.getGalgoAssBalStmt().getBsnsMsg();
            if (CollectionUtils.isEmpty(bsnsMsgList)) {
                log.info("A lista bsnsMsgList retornou vazia");
            } else {
                log.info("{} elemento(s) na lista bsnsMsgList", bsnsMsgList.size());
            }

            StringWriter writer = new StringWriter();
            JAXB.marshal(resp, writer);
            log.debug("resp:\n\n{}\n\n", writer.getBuffer());

        } catch (ConsumirFaultMsg fault) {
            log.info("fault.message: {}", fault.getMessage());
            final MessageExceptionComplexType consumirFault = fault.getFaultInfo().getConsumirFault();
            log.info("consumirFault().getDsException(): {}", consumirFault.getDsException());
            log.info("consumirFault.idException: {}", consumirFault.getIdException());
            log.info("\n\n{}\n\n", posicaoDeAtivos.getRequestResponseString());
        }
        log.info("Fim do teste ***************************************************************");
    }

    @Test
    public void testConsumir_PC0137() throws Exception {
        log.info("Início do teste ***************************************************************");
        final PosicaoDeAtivos posicaoDeAtivos = new PosicaoDeAtivos();
        posicaoDeAtivos.setAmbiente(AmbienteEnum.HOMOLOGACAO).setUser("SG_0306.SG_03", "Galgo111");

        try {
            posicaoDeAtivos.setDt(LocalDate.now()).setMarkers(1L, "12345678901234567890123456789012").setMoeda("BRL")
                .setIdMsgSender("PosicaoDeAtivosTest").addCdObjetos(38172, null, null, null).consumir();
        } catch (ConsumirFaultMsg fault) {
            log.info("fault.message: {}", fault.getMessage());
            final MessageExceptionComplexType consumirFault = fault.getFaultInfo().getConsumirFault();
            log.info("consumirFault().getDsException(): {}", consumirFault.getDsException());
            log.info("consumirFault.idException: {}", consumirFault.getIdException());
            log.info("\n\n{}\n\n", posicaoDeAtivos.getRequestResponseString());
        }
        log.info("Fim do teste ***************************************************************");
    }
}